#include <cstdlib>
#include <iostream>
#include <ctime>
#include <string>
using namespace std;
int main()
{
	const int arraySize = 10;
	int arr[arraySize][arraySize]={};
	for(auto i=0;i<arraySize;++i)
	{
		for (int j = 0; j < arraySize; ++j)
			arr[i][j] = i + j;
	}
	
	for (auto& i : arr)
	{
		for (auto j : i)
		{
			cout.fill(' ');
			cout.width(3);
			cout << j;
		}
			
		cout<< endl;
	}
	time_t timeInTimeT = time(nullptr);
	tm now;
	localtime_s(&now,&timeInTimeT);
	const auto dResult = now.tm_mday % arraySize;
	int result(0);
	//
	string resultArrayAsString;
	for (int j = 0; j < arraySize; ++j)
	{
		result += arr[dResult][j];
		resultArrayAsString+= to_string(arr[dResult][j]);
	}
		
	cout << "Current day = " << now.tm_mday << endl;
	cout << "N = " << arraySize<<endl;
	cout << "(Current day) % N = " << dResult<< endl;
	cout << "N in array is = " << resultArrayAsString << endl;
	cout << "Result = "<<result<<endl;
	//
	return EXIT_SUCCESS;
}
